package listeners;

import gui.DragObject;
import gui.JLine;
import gui.JMComponent;
import gui.JMComponent.Action;
import gui.JSelector;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MMoveListener  implements MouseMotionListener{
	private DragObject dragObject;
	private boolean lineFlag;
	private JLine line;
	private JSelector selector;
	
	public MMoveListener(DragObject dragObject){
		this.dragObject=dragObject;
		lineFlag=false;
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if (dragObject.isStatus()){
			//JMComponent object=dragObject.getObject();
			if (dragObject.getAction()==Action.DRAG){
//				if (object.equals(e.getComponent())){
//					object.setLocation(e.getX()+object.getX()-object.getWidth()/2,e.getY()+object.getY()-object.getHeight()/2);
//				}
//				else {
					dragObject.setLocation(e.getComponent().getX()+e.getX()-dragObject.getShift().x,e.getComponent().getY()+e.getY()-dragObject.getShift().y);
				//}
			}
			else if (dragObject.getAction()==Action.RESIZE){
				JMComponent object=dragObject.getObjects().get(0);

				int x,y;
				if (object.equals(e.getComponent())){
					x=e.getX();
					y=e.getY();
				}
				else{
					x=e.getComponent().getX()+e.getX()-object.getX();
					y=e.getComponent().getY()+e.getY()-object.getY();
				}
				
				
				int w,h;
				if (x<y){
					w=x;
					h=(int) Math.round(x*object.gethRatio());
				}
				else {
					
					h=y;
					w=(int) Math.round(y/object.gethRatio());
				}
				object.setSize(w,h);
				
			}
			return;
		}
		if (lineFlag && line !=null){
			line.update(new Point(e.getX()+e.getComponent().getX(),e.getY()+e.getComponent().getY()));
			return;
		}
		if (selector!=null){
			int x,y;
			x=e.getComponent().getX()+e.getX();
			y=e.getComponent().getY()+e.getY();
			selector.updateGeometry(new Point(x,y));
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (dragObject.isStatus()) {
			dragObject.drop();
		}
			}
	
	public void setLine(JLine line){
		this.line=line;
		lineFlag=true;
	}
	
	public void removeLine(){
		this.line=null;
		lineFlag=false;
	}
	
	public void setSelector(JSelector sel){
		selector=sel;
	}
	
	public void removeSelector(){
		selector=null;
	}

}
