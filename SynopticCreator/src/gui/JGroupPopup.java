package gui;

import interfaces.GroupMenuListener;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import events.GroupEvent;

public class JGroupPopup extends JPopupMenu{
	private static final long serialVersionUID = 1L;
    // Create the listener list
	protected javax.swing.event.EventListenerList listenerList =
		        new javax.swing.event.EventListenerList();

	private ArrayList<JMComponent> elements;
	public JGroupPopup(){
		setElements(new ArrayList<JMComponent>());
		initItems();
	}
	public ArrayList<JMComponent> getElements() {
		return elements;
	}
	private void setElements(ArrayList<JMComponent> components) {
		this.elements = components;
	}
	
	private void initItems(){
		JMenuItem item=new JMenuItem("Create Group");
		add(item);
		item.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GroupEvent evt=new GroupEvent(elements,this);
				for (GroupMenuListener listener : listenerList.getListeners(GroupMenuListener.class)){
					listener.createGroupEvent(evt);
				}
			}});
		
		JMenuItem item2=new JMenuItem("Delete Group");
		add(item2);
		item2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GroupEvent evt=new GroupEvent(elements,this);
				for (GroupMenuListener listener : listenerList.getListeners(GroupMenuListener.class)){
					listener.deleteGroupEvent(evt);
				}
			}});

	}
	
	public void show(Point position,ArrayList<JMComponent> components,Component parent){
		setElements(components);
		super.show(parent,position.x,position.y);
	}

	
	public void addGroupListener(GroupMenuListener listener){
		listenerList.add(GroupMenuListener.class, listener);
	}
	
	public void removeListener(GroupMenuListener listener){
		listenerList.remove(GroupMenuListener.class, listener);
	}
}
