package gui;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RadialGradientPaint;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.Point2D;


public class JDevice extends JMComponent {

	/**
	 * 
	 */	
	private static final long serialVersionUID = 1L;
	
	public JDevice(){
		this.copiable=false;
		setID("#00001");
		setType("device");
	}
	
	public JDevice(boolean copiable){
		this.copiable=copiable;
		setID("#00001");
		setType("device");
	}
	
//	@Override
//	public void setSize(int w,int h){
//		if (w>=getMinWidth() && w<=getMaxHeight() && h>=getMinHeight() && h<=getMaxHeight()){
//			super.setBounds(getX(), getY(),getStep()*(int)Math.round(w/(double)getStep()), getStep()*(int)Math.round(w/(double)getStep())+27);
//			for (JLineConnection line:  getLines()){
//				line.update();
//			}	
//		}
//	}
	
	
	public void paint(Graphics g){
		Graphics2D g2=(Graphics2D)g;
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		
		if (isSelected()) this.drawSelectionBorder(g2);
		
		
		
		if (this.getCourrentAction()==Action.NOTHING)
			g2.setColor(Color.yellow);
		else if (getCourrentAction()==Action.DRAG && getX()<70)
			g2.setColor(new Color(255,0,0,50));
		else
			g2.setColor(new Color(255,255,0,50));
		
		g2.fillRoundRect(5,5, this.getWidth()-10,this.getWidth()-10,8,8);
	    Point2D center = new Point2D.Float(getWidth()/2, getWidth()/2-getWidth()/4);
	    float radius = (float) (getWidth()*1.5);
	    float[] dist = {0.0f, 1.0f};
	    Color[] colors = {new Color(116,117,0,0),new Color(116,117,0,255)};
	    RadialGradientPaint p =
	         new RadialGradientPaint(center, radius, dist, colors);
	     
	    g2.setPaint(p);
	    g2.fillRoundRect(5,5, this.getWidth()-10,this.getWidth()-10,8,8);
	    
	    
		
		
		g2.setColor(new Color(144,146,0));
		g2.setStroke(new BasicStroke((float) 1.5));
	
		g2.drawRoundRect(5,5, this.getWidth()-10,this.getWidth()-10,8,8);
		Font font=new Font("URW Gothic L",Font.BOLD,14);//"Nimbus Mono L",Font.BOLD,12);
		
		g2.setFont(font);
		FontRenderContext frc= new  FontRenderContext(g2.getTransform(),true,false);
		int dx=(int)Math.round((getWidth()-font.getStringBounds("DEV",frc).getWidth())/2.0);
		
		g2.drawString("DEV", dx, 22);
		this.drawLabel(g2);

		this.drawOverlay(g2);
	}	
	
	@Override
	public JMComponent copy(){
		JDevice copy=new JDevice(false);
		copy.setLocation(getX(),getY());
		copy.setSize(getRealWidth(), getRealHeight());
		return copy;
	}

	@Override
	public Point getLeft(){
		return new Point(getX()+5,getY()+getWidth()/2);
	}
	
	@Override
	public Point getRight(){
		return new Point(getX()+getWidth()-5,getY()+getWidth()/2);
	}


	@Override
	public Point getUp(){
		return new Point(getX()+getWidth()/2,getY()+5);
	}
	
	@Override
	public Point getDown(){
		return new Point(getX()+getWidth()/2,getY()+getWidth()-5);
	}
}
