package gui;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import gui.JMComponent.Action;



public class DragObject  {
	private ArrayList<JMComponent> objects;
	private boolean status;
	private Point shift;
	
	public DragObject(){
		objects=new ArrayList<JMComponent>();
		status=false;
		shift=new Point(0,0);
	}

	public ArrayList<JMComponent> getObjects() {
		return objects;
	}

	public void drag(JMComponent object,Point dragPoint) {
		if (object!=null) {
			objects.add(object);
			status=true;
			object.startAction(Action.DRAG);
			shift=new Point(dragPoint.x-getX(),dragPoint.y-getY());
		}
		
	}
	
	public void drag(ArrayList<JMComponent> objects,Point dragPoint) {
		for (JMComponent object : objects)
			drag(object,dragPoint);
		
	}
	
	
	public void resize(JMComponent object){
		drop();
		if (object!=null) {
			objects.add(object);
			status=true;
			object.startAction(Action.RESIZE);
		}
	}

	public void drop(){
		shift=new Point(0,0);
		status=false;
		for (JMComponent object : objects)
			object.stopAction();
		objects.clear();		
	}
	
	public boolean isStatus(){
		return status;
	}
	
	public Action getAction(){
		if (status==false || objects.size()<1) return Action.NOTHING;
		return objects.get(0).getCourrentAction();
	}
	
	public int getX(){
		if (objects.size()<1) return 0;
		else{
			int x=objects.get(0).getX();
			for (JMComponent comp : objects){
				if (x>comp.getX())
					x=comp.getX();
			}
			return x;
		}
	}
	public int getY(){
		if (objects.size()<1) return 0;
		else{
			int y=objects.get(0).getY();
			for (JMComponent comp : objects){
				if (y>comp.getY())
					y=comp.getY();
			}
			return y;
		}
	}
	public int getWidth(){
		if (objects.size()<1) return 0;
		else{
			int w=objects.get(0).getX()-getX()+objects.get(0).getWidth();
			for (JMComponent comp : objects){
				if (w<comp.getX()-getX()+comp.getWidth())
					w=comp.getX()-getX()+comp.getWidth();
			}
			return w;
		}
	}
	public int getHeight(){
		if (objects.size()<1) return 0;
		else{
			int h=objects.get(0).getY()-getY()+objects.get(0).getHeight();
			for (JMComponent comp : objects){
				if (h<comp.getY()-getY()+comp.getHeight())
					h=comp.getY()-getY()+comp.getHeight();
			}
			return h;
		}
	}

	public void setLocation(int x, int y){
		int dx,dy;
		dx=x-getX();
		dy=y-getY();
		for (JMComponent object : objects)
			object.setLocation(dx+object.getX(),dy+object.getY());
		
	}
	
	public Point getShift(){
//		if (objects.size()==1){
//			return new Point(getWidth()/2,getHeight()/2);
//		}
//		else 
//			return new Point(10,10);
//		
		return shift;
	}

	public Rectangle getRect() {
	
		return new Rectangle(getX(),getY(),getWidth(),getHeight());
	}
}
