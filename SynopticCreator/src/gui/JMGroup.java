package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;

public class JMGroup extends JMComponent{
	private static final long serialVersionUID = 1L;
	private ArrayList <JMComponent> elements;
	//private ArrayList <JLineConnection> lines;
	private boolean showElement;
	public JMGroup(){
		elements=new ArrayList<JMComponent>();
		this.showLabel(false);
		showElement=true;
		copiable=false;
	}

	private void updateBounds(JMComponent element){
		
		int x,y;
		if (elements.size()>1){
			x=getX();
			y=getY();
		
			if (element.getX()<x) 
				x=element.getX()-getStep();
			if (element.getY()<y)
				y=element.getY()-getStep();
		}
		else {
			x=element.getX()-getStep();
			y=element.getY()-getStep();
		}
		
		int w,h;
		w=getWidth();
		h=getHeight();
		
		if (element.getX()-x+element.getRealWidth()>w)
			w=element.getX()-x+element.getRealWidth()+getStep();
		if (element.getY()-y+element.getRealHeight()>h)
			h=element.getY()-y+element.getHeight()+getStep();
		
		h=(int)(getStep()*Math.round((double)h/(double)getStep())+getStep());
		
		this.realHeight=h;
		this.realWidth=w;
		this.setBounds(x,y,w,h);			
		
	}
	
	public void addElement(JMComponent element){
		elements.add(element);
		add(element);
		element.showLabel(true);
		element.setEnabled(false);
		updateBounds(element);
		//element.setBounds(element.getX()-getX(), element.getY()-getY(), element.getWidth(), element.getHeight());
	}
	
	public void addElements( ArrayList<JMComponent> list){
		for (JMComponent comp: list)
			addElement(comp);
		
	}
	
	public ArrayList<JMComponent> getElements() {
		return elements;
	}

	public boolean isShowElement() {
		return showElement;
	}


	public void setShowElement(boolean showElement) {
		this.showElement = showElement;
	}
	
	
	public void paint(Graphics g){
		Graphics2D g2 =(Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		
		
		if (isSelected())
			super.drawSelectionBorder(g2);
		
		g2.setColor(new Color(72,62,55,180));
		g2.fillRoundRect(0,0, getWidth(), getHeight(),15,15);
		g.setColor(new Color(186,186,186,200));
		g.drawRoundRect(0,0, getWidth()-1, getHeight()-1,15,15);
		
		
		g.translate(-getX(), -getY());
		if (showElement){
			super.paint(g);
		}
	}

	@Override
	public void setLocation(int x, int y){
		int dx,dy;
		dx=x-getX();
		dy=y-getY();
		super.setLocation(x, y);
		for (JMComponent element : elements)
			element.setLocation(element.getX()+dx,element.getY()+dy);
		
	}
}

