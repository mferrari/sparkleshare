package gui;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;


public class JValve extends JMComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public JValve(){
		copiable=false;
		setType("valve");
		setID("#0000");
	}
	
	public JValve(boolean copiable){
		this.copiable=copiable;
		setType("valve");
		setID("#0000");
	}
	
	public void paint(Graphics g){
		Graphics2D g2=(Graphics2D)g;
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		
		if 	(isSelected())
			this.drawSelectionBorder(g2);
		
		Polygon poly=new Polygon();
		
		poly.addPoint(12, 8);
		poly.addPoint(getRealWidth()-12, 8);
		poly.addPoint(12, getRealHeight()-8);
		poly.addPoint(getRealWidth()-12, getRealHeight()-8);
		
		if (getCourrentAction()==Action.NOTHING)
			g2.setColor(new Color(205,92,92));
		else if (getCourrentAction()==Action.DRAG && getX()<70)
			g2.setColor(new Color(255,0,0,50));
		else
			g2.setColor(new Color(205,92,92,50));
		g2.fillPolygon(poly);
		
		g2.setColor(Color.black);
		g2.drawPolygon(poly);
		this.drawLabel(g2);
		this.drawOverlay(g2);
	}	
	
	@Override
	public Point getLeft(){
		return new Point(getX()+getRealWidth()/2,getY()+getRealHeight()/2);
	}
	
	
	@Override
	public Point getRight(){
		return getLeft();
	}
	
	@Override
	public Point getUp(){
		return getLeft();
	}
	@Override
	public Point getDown(){
		return getLeft();
	}
	
	@Override
	public JMComponent copy(){
		JValve copy=new JValve(false);
		copy.setLocation(getX(),getY());
		copy.setSize(getRealWidth(), getRealHeight());
		return copy;
	}
	
}
