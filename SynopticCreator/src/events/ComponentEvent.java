package events;

import gui.JMComponent;

import java.util.EventObject;

public class ComponentEvent  extends EventObject{
	private static final long serialVersionUID = 1L;
	private JMComponent component;

	public ComponentEvent(JMComponent component,Object source) {
		super(source);
		this.component=component;
	}

	public JMComponent getComponent() {
		return component;
	}
}
