package events;

import gui.JLineConnection;

import java.util.EventObject;

public class LineEvent extends EventObject{
	private static final long serialVersionUID = 1L;
	private JLineConnection line;
	public LineEvent(JLineConnection line, Object source) {
		super(source);
		this.line=line;
	}
	public JLineConnection getLine() {
		return line;
	}
}
