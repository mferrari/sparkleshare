package interfaces;

import java.util.EventListener;

import events.LineEvent;

public interface LineMenuListener extends EventListener {
	public void deleteEventOccurred(LineEvent evt);
}
